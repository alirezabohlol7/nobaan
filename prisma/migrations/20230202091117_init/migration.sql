-- CreateTable
CREATE TABLE `users` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `full_Name` VARCHAR(191) NULL,
    `email` VARCHAR(191) NULL,
    `number` INTEGER NULL,
    `national_Code` INTEGER NULL,
    `count_visit` INTEGER NULL,
    `country` VARCHAR(191) NULL,
    `city` VARCHAR(191) NULL,
    `gender` VARCHAR(191) NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    UNIQUE INDEX `users_email_key`(`email`),
    UNIQUE INDEX `users_number_key`(`number`),
    UNIQUE INDEX `users_national_Code_key`(`national_Code`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `dockters` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `fsPath` VARCHAR(191) NULL,
    `fullName` VARCHAR(191) NULL,
    `biography` VARCHAR(191) NULL,
    `email` VARCHAR(191) NOT NULL,
    `number` INTEGER NULL,
    `expertise` VARCHAR(191) NULL,
    `MSN` INTEGER NULL,
    `Capacity` INTEGER NULL,
    `gender` VARCHAR(191) NULL,
    `country` VARCHAR(191) NULL,
    `city` VARCHAR(191) NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    UNIQUE INDEX `dockters_fsPath_key`(`fsPath`),
    UNIQUE INDEX `dockters_email_key`(`email`),
    UNIQUE INDEX `dockters_number_key`(`number`),
    UNIQUE INDEX `dockters_MSN_key`(`MSN`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `turns` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `userId` INTEGER NOT NULL,
    `dockerId` INTEGER NOT NULL,
    `time` DATETIME(3) NULL,
    `price` DOUBLE NULL,
    `type_Disease` VARCHAR(191) NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,

    UNIQUE INDEX `turns_userId_key`(`userId`),
    UNIQUE INDEX `turns_dockerId_key`(`dockerId`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `turns` ADD CONSTRAINT `turns_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `turns` ADD CONSTRAINT `turns_dockerId_fkey` FOREIGN KEY (`dockerId`) REFERENCES `dockters`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
