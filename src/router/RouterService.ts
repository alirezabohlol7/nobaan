import { Application, Router } from "express";
import RouterEngine from "./router";
import ticketRouter from "../Ticket/TicketRouter"
import App from "src/app";


export default class RouterService {
    public app: Application
    public router: RouterEngine
    constructor(app: Application) {
        this.app = app
        this.router = new RouterEngine()
        this.bindRouters()
    }

    public bindRouters() {
        this.router.registerRouter('/getTicket', ticketRouter)
    }
    public run(){
        this.router.getRouters().forEach((router: Router, route: string) => {
           return this.app.use(route, router)
        })
    }

}