import * as express from 'express'
import { Application } from 'express'
import boot from './boot';
import RouterService from './router/RouterService';

class App {
  public readonly app : Application
  public readonly port: number;
  public readonly router: RouterService;
  constructor(port: number) {
    this.app = express()
    this.port = port
    this.router = new RouterService(this.app)
  }

  public start(): void {
    boot(this.app)
    this.router.run()
    this.app.listen(this.port, () => {
     return console.log(`app instance is running on port: ${this.port}`)
    })
  }
}

export default App