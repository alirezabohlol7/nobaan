import PrismaService from "../prisma/PrismaService"
export default class TicketService {
    public prismaService: PrismaService
    constructor() {
        this.prismaService = new PrismaService()
    }
    public async registerTicket(reqBody: { userId: number, dockerId: number }) {
        try {
            const { userId, dockerId } = reqBody
            const result = await this.prismaService.turns.create({
                data: {
                    userId,
                    dockerId
                }
            })
            return result
        } catch (error) {
            console.log(error)
        }

    }
} 
