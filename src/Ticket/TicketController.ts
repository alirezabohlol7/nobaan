import { Request, Response } from "express"
import PrismaService from "../prisma/PrismaService"
import TicketService from "./TicketService"
export default class TicketController {
    public ticketService: TicketService
    public prismaService: PrismaService
    constructor() {
        this.prismaService = new PrismaService
        this.ticketService = new TicketService()
        this.getTicket = this.getTicket.bind(this);
    }
    public async getTicket(req: Request, res: Response) {
        try {
            const reqBody: { userId: number, dockerId: number } = req.body
            const { userId, dockerId } = reqBody
            // validate request for ticket
            const validateUser = await this.prismaService.turns.findUnique({
                where:{
                   userId
                }
            })
            if (validateUser) {
                if (validateUser.dockerId === dockerId) {
                    return res.send({
                        error: true,
                        errorMessage: 'Credentials Mismatched'
                    })
                }
            }
           // register ticket user
            const createMessage = this.ticketService.registerTicket(reqBody)
            if (!createMessage) {
                return res.send({
                    error: true,
                    message:"Credentials Mismatched"
                })
            }
            return res.send({
                success: true,
                createMessage
            })
        } catch (error) {
            console.log(error)
        }
    }
}
