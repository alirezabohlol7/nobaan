import { Router } from "express"
import TicketController from "./TicketController"
const ticketController = new TicketController()
const router:Router = Router() 

router.post('/',ticketController.getTicket)

export default router

