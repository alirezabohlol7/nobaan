import { Application } from 'express'
import * as bodyParser from 'body-parser'
export default function boot (app:Application) {
 return app.use(bodyParser.json())
}